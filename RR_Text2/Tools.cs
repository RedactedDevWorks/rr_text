﻿using System;

using System.Reflection;
using System.Media;

namespace RR_Text2
{
    class Tools
    {
        public static bool any_phrase(string source, string[] phrases)
        {
            foreach (string phrase in phrases)
                if (source.Contains(phrase))
                    return true;

            return false;
        }

        private static readonly Random rnd = new Random();
        public static int roll(int max)
        {
            return rnd.Next(1, max);
        }

        public static Random getRandom()
        {
            return rnd;
        }

        public static void Story(string text, bool waitForInput = true)
        {
            Console.WriteLine(text);
            if (waitForInput)
                Console.ReadKey();
        }

        public static void Dialog(string dialog, string action)
        {
            Console.WriteLine("\n\"" + dialog + "\" " + action + "\n");
            Console.ReadKey();
        }

        public static string Action(string options)
        {
            Console.WriteLine("\n\n" + options + "\n");
            Console.Write("Action: ");
            return Console.ReadLine();
        }

        public static string Action(string[] options)
        {
            int i = 0;
            foreach (string option in options)
            {
                i++;
                Console.WriteLine(i + ")" + option);
            }

            Console.Write("Action: ");
            return Console.ReadLine();
        }

        private static WMPLib.WindowsMediaPlayer player = new WMPLib.WindowsMediaPlayer();
        private static WMPLib.WindowsMediaPlayer ambiance = new WMPLib.WindowsMediaPlayer();
        public static void PlaySound(string file)
        {
            player.URL = "./res/audio/" + file;
            player.controls.play();
        }
        public static void PlayAmbience(string file)
        {
            ambiance.settings.setMode("loop", true);
            ambiance.URL = "./res/audio/" + file;
            ambiance.controls.play();
        }
        public static void StopAmbience()
        {
            ambiance.controls.stop();
        }
    }
}
