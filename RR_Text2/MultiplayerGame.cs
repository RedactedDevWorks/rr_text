﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace RR_Text2
{
    class MultiplayerGame
    {
        public const string ROCK = "rock";
        public const string PAPER = "paper";
        public const string SCISSORS = "scissors";

        private const int TIE = 1;
        private const int WIN = 2;
        private const int LOSE = 3;

        private const int PLAYER_CLIENT = 1;
        private const int PLAYER_SERVER = 2;

        private string name;

        protected readonly Int32 game_port = 13000;

        private TcpListener server = null;
        private TcpClient client = null;


        // Game vars
        private string opponent_name;




        public void start()
        {
            if (this.name == null || this.name == "")
                init();
            else
                actions();
        }

        public void init()
        {
            for(;;)
            {
                Console.Write("What's your name? ");
                this.name = Console.ReadLine();

                Console.WriteLine("Your name is " + this.name + "? Is that ok?");
                string option = Tools.Action(new string[] { "Yes", "No" });

                if (Tools.any_phrase(option, new string[] { "1", "y", "yes", "Yes", "YES", "go" }))
                    break;
            }

            actions();

            
        }

        private void actions()
        {
            string action;
            for (;;)
            {
                // Ask for IP to connect to
                Console.WriteLine("OK "+ this.name +", What would you like to do?");
                action = Tools.Action(new string[] { "Connect to Game", "Host New Game" });

                if (Tools.any_phrase(action, new string[] { "1", "connect" }))
                {
                    action = "connect";
                    break;
                }
                else if (Tools.any_phrase(action, new string[] { "2", "host" }))
                {
                    action = "host";
                    break;
                }
                else
                {
                    Tools.Story("Not quite sure what you said, please try again");
                }
                // attempt to connect to remote client

                
            }

            switch (action)
            {
                case "connect":
                    mp_connect();
                    break;
                default:
                case "host":
                    mp_host();
                    break;
            }
        }

        

        public static void GetHostAddresses(string hostname)
        {
            IPAddress[] ips;

            ips = Dns.GetHostAddresses(hostname);

            Console.WriteLine("GetHostAddresses({0}) returns:", hostname);

            foreach (IPAddress ip in ips)
            {
                Console.WriteLine("    {0}", ip);
            }
        }

        public string GetLocalHostName()
        {
            try
            {
                // Get the local computer host name.
                String hostName = Dns.GetHostName();
                Console.WriteLine("Computer name :" + hostName);
                return hostName;
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException caught!!!");
                Console.WriteLine("Source : " + e.Source);
                Console.WriteLine("Message : " + e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception caught!!!");
                Console.WriteLine("Source : " + e.Source);
                Console.WriteLine("Message : " + e.Message);
            }

            return "localhost";
        }


        private void mp_connect()
        {
            for(;;)
            {
                try
                {
                    Console.Write("IP Address to Connect to: ");
                    string remote = Console.ReadLine();
                    this.client = new TcpClient(remote, this.game_port);
                    break;
                }
                catch (ArgumentNullException e)
                {
                    Console.WriteLine("ArgumentNullException: {0}", e);
                }
                catch (SocketException e)
                {
                    Console.WriteLine("SocketException: {0}", e);
                }
            }

            // Run Game
            
            sendMessage("sendname", this.name);
            for(;;)
            {
                Message opponentName = waitForMessage();
                if (opponentName != null && opponentName.header == "sendname")
                {
                    this.opponent_name = opponentName.body;
                    break;
                }
            }

            Console.Write("Your opponents name is {0}", this.opponent_name);

            newGame();

            client.Close();
        }

        private void mp_host()
        {
            try
            {
                this.server = new TcpListener(IPAddress.Any, this.game_port);

                // start listening for requests
                this.server.Start();

                for(;;)
                {
                    GetHostAddresses(GetLocalHostName());

                    Console.WriteLine("Waiting for connection... Your IP address is ");

                    this.client = server.AcceptTcpClient();
                    Console.WriteLine("Connected!");
                    break;
                }

                sendMessage("sendname", this.name);
                for(;;)
                {
                    Message opponentName = waitForMessage();
                    if (opponentName != null && opponentName.header == "sendname")
                    {
                        this.opponent_name = opponentName.body;
                        break;
                    }

                }
                Console.WriteLine("Your opponents name is {0}, just so you know", this.opponent_name);

            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }

            newGame();

            // Run Game

            this.server.Stop();
            client.Close();
        }

        public struct GameState
        {
            public int activeChamber;
            public int chambers;
            public int bulletChamber;
            public int turns;
            public int activePlayer;
        };
        GameState state;
        

        private void newGame()
        {
            state = new GameState();

            Console.WriteLine("\n========================\nWelcome to certain death!\n\nHost sets up the gun..");
            // server sets up the game
            if (this.server != null)
            {
                state.turns = 0;
                state.chambers = 7;
                state.bulletChamber = Tools.roll(state.chambers); // choose a random chamber to have the bullet
                spinClip();

                sendMessage("setupgame", JsonConvert.SerializeObject(state));

            } else
            {
                // wait for sound and setup instructions
                for(;;)
                {
                    Message msg = waitForMessage();
                    if(msg != null && msg.header == "playsound")
                    {
                        Tools.PlaySound(msg.body);
                    } 
                    else if(msg != null && msg.header == "setupgame")
                    {
                        GameState setup = JsonConvert.DeserializeObject<GameState>(msg.body);
                        if(setup.chambers > 0)
                        {
                            state = setup;
                        }
                        break;
                    }
                }
            }
            
            Console.WriteLine("The loser takes the chamber.");

            for(;;)
            {
                // do the steps
                doRPS();
                if(state.activePlayer != 0)
                {
                    doRR();
                }
            }
            
        }

        private bool pullTrigger()
        {
            // rotate chamber
            state.activeChamber++;
            if (state.activeChamber > state.chambers)
                state.activeChamber = 1;

            // Is the bullet in this chamber?
            bool isBulletChamber = (state.activeChamber == state.bulletChamber);

            // queue sound
            if (isBulletChamber)
                Tools.PlaySound("wet.wav");
            else
                Tools.PlaySound("dry.wav");

            return isBulletChamber;
        }

        private void doRR()
        {
            // if it's our turn
            if ((this.server != null && state.activePlayer == PLAYER_SERVER) || (this.server == null && state.activePlayer == PLAYER_CLIENT))
            {
                for (;;)
                {
                    string action = Tools.Action(new string[] { "Spin the clip?", "Fire the gun?" });

                    if (Tools.any_phrase(action, new string[] { "1", "spin", "clip", "chamber" }))
                    {
                        if (this.server != null)
                        {
                            spinClip();
                            sendMessage("updateactivechamber", state.activeChamber.ToString());
                        } 
                        else
                        {
                            sendMessage("request", "clipspin");
                            for (;;)
                            {
                                Message msg = waitForMessage();
                                if (msg != null && msg.header == "updateactivechamber")
                                {
                                    int newchamber;
                                    bool parsed = Int32.TryParse(msg.body, out newchamber);
                                    if(parsed)
                                    {
                                        state.activeChamber = newchamber;
                                    }
                                    break;
                                }
                            }
                        }    
                    }
                    else if (Tools.any_phrase(action, new string[] { "2", "trigger", "fire", "shoot" }))
                    {
                        if(this.server != null)
                        {
                            bool dead = pullTrigger();
                            if(dead)
                            {
                                Console.WriteLine("You dead homie");
                                sendMessage("endround", "finish");
                                endgameLose();
                                break;
                            }
                            else
                            {
                                Console.WriteLine("You alive you beautiful bastard");
                                sendMessage("endround", "continue");
                                break;
                            }
                        } else
                        {
                            sendMessage("request", "trigger");
                            for (;;)
                            {
                                Message msg = waitForMessage();
                                if (msg != null && msg.header == "updatetrigger")
                                {
                                    if(msg.body == "dead")
                                    {
                                        Console.WriteLine("Sorry homie, you dead");
                                        sendMessage("endround", "finish");
                                        endgameLose();
                                        break;
                                    } 
                                    else
                                    {
                                        Console.WriteLine("Yay you alive!");
                                        sendMessage("endround", "continue");
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Could not understand your action, try again");
                    }
                }
            }
            // otherwise just wait for messages
            else
            {
                for (;;)
                {
                    Message msg = waitForMessage();
                    if (msg != null && msg.header == "playsound")
                    {
                        Tools.PlaySound(msg.body);
                    }
                    else if(msg != null && msg.header == "updateactivechamber")
                    {
                        Console.WriteLine("{0} has decided to spin the chamber", opponent_name);
                        int newchamber;
                        bool parsed = Int32.TryParse(msg.body, out newchamber);
                        if (parsed)
                        {
                            state.activeChamber = newchamber;
                        }
                    }
                    else if(msg != null && msg.header == "request")
                    {
                        if(msg.body == "clipspin")
                        {
                            Console.WriteLine("{0} has decided to spin the clip", opponent_name);
                            spinClip();
                            sendMessage("updateactivechamber", state.activeChamber.ToString());
                        }
                        else if(msg.body == "trigger")
                        {
                            Console.WriteLine("{0} has decided to pull the trigger", opponent_name);
                            bool trigger = pullTrigger();
                            sendMessage("updatetrigger", trigger ? "dead" : "alive");
                        }
                    }
                    else if (msg != null && msg.header == "endround")
                    {
                        if(msg.body == "finish")
                        {
                            Console.WriteLine("{0} took the bullet like a champ.", opponent_name);
                        } else if(msg.body == "continue")
                        {
                            Console.WriteLine("{0} lives to see another round", opponent_name);
                            break;
                        }
                    }
                }
            }
        }

        private void doRPS()
        {
            state.activePlayer = 0;
            string mychoice;
            string oppchoice;
            for (;;)
            {
                for (;;)
                {
                    string action = Tools.Action(new string[] { "Rock", "Paper", "Scissors" });
                    if (Tools.any_phrase(action, new string[] { "1", "r", "rock" }))
                    {
                        mychoice = MultiplayerGame.ROCK;
                        Console.WriteLine("You chose Rock! Smashing choice!");
                        break;
                    }
                    else if (Tools.any_phrase(action, new string[] { "2", "p", "paper" }))
                    {
                        mychoice = MultiplayerGame.PAPER;
                        Console.WriteLine("You chose Paper! How creative!");
                        break;
                    }
                    else if (Tools.any_phrase(action, new string[] { "3", "s", "scissors" }))
                    {
                        mychoice = MultiplayerGame.SCISSORS;
                        Console.WriteLine("You chose Scissors! How cutting edge!");
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Could not understand your input, try again");
                    }
                }

                sendMessage("rps", mychoice);
                for (;;)
                {
                    Message msg = waitForMessage();
                    if (msg != null && msg.header == "rps")
                    {
                        oppchoice = msg.body;
                        break;
                    }
                }

                Console.WriteLine("You chose {0}, and {1} chose {2}", mychoice, this.opponent_name, oppchoice);
                int decision = decide(mychoice, oppchoice);

                if (decision == TIE)
                {
                    Console.WriteLine("It's a tie! Try again..");
                }
                else if (decision == LOSE)
                {
                    Console.WriteLine("Oh dear, you lost. Let's do this thing...");
                    state.activePlayer = this.server != null ? PLAYER_SERVER : PLAYER_CLIENT;
                    break;
                }
                else if (decision == WIN)
                {
                    Console.WriteLine("You Win! Your opponent now takes their chance...");
                    state.activePlayer = this.server != null ? PLAYER_CLIENT : PLAYER_SERVER;
                    break;
                }
            }
        }

        

        // RPG Decision Engine
        private int decide(string my, string opp)
        {
            if (my == opp)
            {
                return TIE;
            }
            else if (my == ROCK)
            {
                if (opp == PAPER)
                    return LOSE;
                else if (opp == SCISSORS)
                    return WIN;
            }   
            else if (my == PAPER)
            {
                if (opp == SCISSORS)
                    return LOSE;
                else if (opp == ROCK)
                    return WIN;
            }
            else if (my == SCISSORS)
            {
                if (opp == ROCK)
                    return LOSE;
                else if (opp == PAPER)
                    return WIN;
            }

            return TIE;
        }

        private void spinClip()
        {
            //sendMessage("playsound", "clip_spin.wav");
            state.activeChamber = Tools.roll(state.chambers); // choose random chamber to start on
        }

        /*
        * TCP Messaging methods
        */

        public void sendMessage(string header, string body)
        {
            Message msg = new Message();
            msg.header = header;
            msg.body = body;

            string message = JsonConvert.SerializeObject(msg);

            Byte[] data = Encoding.ASCII.GetBytes(message);
            NetworkStream stream = this.client.GetStream();
            stream.Write(data, 0, message.Length);
        }

        public Message waitForMessage()
        {
            NetworkStream stream = this.client.GetStream();

            byte[] readBuffer = new byte[1024];
            StringBuilder completeMessage = new StringBuilder();
            int bytesRead = 0;

            do {
                bytesRead = stream.Read(readBuffer, 0, readBuffer.Length);
                completeMessage.AppendFormat("{0}", Encoding.ASCII.GetString(readBuffer, 0, bytesRead));
            } while (stream.DataAvailable);

            Message msg = JsonConvert.DeserializeObject<Message>(completeMessage.ToString());

            return msg;
        }

        public void endgameLose()
        {
            Console.WriteLine("You lost I think, I dunno");
        }
    }
}
