﻿using System;

namespace RR_Text2
{
    class Game
    {
        const int BORIS = 1;
        const int PLAYER = 2;

        int activeChamber;
        int chambers;
        int bulletChamber;
        int turns;
        int activePlayer;

        public void start()
        {
            // roll to see who goes first
            if (Tools.roll(20) >= 10)
                activePlayer = BORIS;
            else
                activePlayer = PLAYER;

            turns = 0;
            chambers = 7;
            bulletChamber = Tools.roll(chambers); // choose a random chamber to have the bullet
            spinClip();

            play();
        }

        private void spinClip()
        {
            Tools.PlaySound("clip_spin.wav");
            activeChamber = Tools.roll(chambers); // choose random chamber to start on
        }

        private bool pullTrigger()
        {
            // rotate chamber
            activeChamber++;
            if (activeChamber > chambers)
                activeChamber = 1;

            // Is the bullet in this chamber?
            bool isBulletChamber = (activeChamber == bulletChamber);

            // queue sound
            if (isBulletChamber)
                Tools.PlaySound("wet.wav");
            else
                Tools.PlaySound("dry.wav");

            return isBulletChamber;
        }

        private void play()
        {
            if (activePlayer != PLAYER)
                turn_computer();
            else
                turn_player();
        }

        private void turn_computer()
        {
            if (chambers == 7 && turns == 0)
            {
                Tools.Dialog("On second thought, why let you have all the fun, I'll go first", "the man chuckles out loud while ripping the revolver out of your hand.");
            }
            turns++;

            Tools.Story("The man grasps hold of the revolver, looking at you with wild rage.");
            if (Tools.roll(20) >= 15)
            {
                spinClip();
                Tools.Story("The man chooses to spin the clip this time around.");
            }
            Tools.PlaySound("cocking.wav");
            Tools.Story("He grins, and pulls the trigger quickly");

            if (pullTrigger())
            {
                Console.ReadKey();
                playerWin();
            }
            else
            {
                Console.ReadKey();
                Tools.PlaySound("slam.wav");
                Tools.Story("The man slams the gun back down on the table");
                Tools.Dialog("You see that? That is how it is done friend.", "he said definitively. ");
                Tools.Dialog("Now it is your turn..", "");
                rotatePlayer();
                play();
            }


        }

        private void turn_player()
        {
            turns++;
            bool cocked = false;
            Tools.Story("The gun is currently in your hand. What do you do?\n", false);
            for (;;)
            {
                string action = Tools.Action(new string[] { "Spin the clip?", "Cock the pin?", "Fire the gun?", "Try to escape?" });

                if (Tools.any_phrase(action, new string[] { "1", "spin", "clip", "chamber" }))
                {
                    spinClip();
                    Tools.Story("You choose to spin the clip.. hoping it will improve your odds");
                }
                else if (Tools.any_phrase(action, new string[] { "2", "cock", "pin", "ready" }))
                {
                    if (!cocked)
                    {
                        cocked = true;
                        Tools.PlaySound("cocking.wav");
                        Tools.Story("You cock the hammer...");
                    }
                    else
                    {
                        Tools.Story("The gun is already cocked. You just kind of look like a jerk trying to buy time now..");
                    }
                }
                else if (Tools.any_phrase(action, new string[] { "4", "escape", "leave" }))
                {
                    Tools.Story("You attempt to get up, but the guy right behind you puts his own metal to the back of your skull");
                    Tools.Dialog("Get down, and stay down.", "He commands");
                    Tools.Story("You obey..");
                }
                else if (Tools.any_phrase(action, new string[] { "3", "trigger", "fire", "shoot" }))
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Could not understand your action, try again");
                }
            }

            // attempt your shot
            Tools.Story("You put the barrel up against your head, and stare directly at the man in front of you...");
            if (!cocked)
                Tools.PlaySound("cocking.wav");

            Tools.Story("And take a shot!");
            if (pullTrigger())
            {
                computerWin();
            }
            else
            {
                Console.ReadKey();
                Tools.Story("Relief instantly overtakes your body as you drop the gun.");
                Tools.Dialog("We're not through yet comrade. My turn.", "the man across from you thunders out loud.");
                rotatePlayer();
                play();
            }

        }


        private void rotatePlayer()
        {
            if (activePlayer == BORIS)
                activePlayer = PLAYER;
            else
                activePlayer = BORIS;
        }


        private void playerWin()
        {
            Tools.Story("The look of shock on the mans never arrived, as the right side of his face became a horror story in the blink of an eye.");
            Tools.PlaySound("head_hit_table.wav");
            Tools.Story("He collapses to the table");
            Tools.PlaySound("body_thud.wav");
            Tools.Story("His lifeless corpse then falls to the ground");
            Tools.Dialog("I.. I won?", "you say to yourself");
            Tools.PlaySound("cocking.wav");
            Console.ReadKey();
            Tools.Dialog("You're not going anywhere", "the man behind you spits through his teeth");
            Tools.PlaySound("wet.wav");
            Tools.StopAmbience();
            Console.ReadKey();
            Tools.Story("The impact from behind throws your head backwards, as a gory spray of crimson erupts from a fresh exit wound");
            Tools.Story("You should have known, there was no escape..");
            Tools.PlaySound("body_thud.wav");
        }

        private void computerWin()
        {
            Tools.StopAmbience();
            Tools.Story("\nIt feels as though you've been hit with a sharp punch, as your vision fills with red.");
            Tools.Story("You remain alive just long enough to see a quick smile overcome your opponent.");
            Tools.Story("Yes.. this, is how you die.");
            Tools.PlaySound("body_thud.wav");
            Console.ReadKey();
        }
    }
}
