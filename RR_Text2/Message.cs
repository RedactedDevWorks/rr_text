﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RR_Text2
{
    class Message
    {
        public string header { get; set; }
        public string body { get; set; }
    }
}
