﻿using System;

namespace RR_Text2
{
    class Program
    {
        static void Main(string[] args)
        {
            Tools.PlayAmbience("bg.wav");
            Tools.Story("A revolver is placed right in front of you. What do you do?", false);
            string action;
            for(;;)
            {
                action = Tools.Action(new string[] {"Pick up the gun?", "Try to escape?", "Look for a real opponent?"});

                if (Tools.any_phrase(action, new string[] { "1", "take", "pick", "up", "gun", "pickup", "grab" }))
                    action = "take";
                else if (Tools.any_phrase(action, new string[] { "2", "get up", "stand up", "escape", "leave", "exit" }))
                    action = "escape";
                else if (Tools.any_phrase(action, new string[] { "3", "multiplayer", "mp", "connect", "look", "real" }))
                    action = "mp";
                else
                {
                    Console.WriteLine("I have no idea what you just said, try again.");
                    action = null;
                }

                if (action != null)
                    break;

            }

            if(action == "escape")
            {
                Tools.Story("You attempt to get up from where you sit");
                Tools.Story("You feel something cold press against your temple. ");
                Tools.PlaySound("cocking.wav");
                Tools.Story("And with one click, your world goes dark.");
                Tools.PlaySound("wet.wav");
                Tools.StopAmbience();
            } else if(action == "take")
            {
                Tools.Story("Nervously, you pick up the ancient looking firearm");
                Tools.Dialog("That is a Nagant M1895 Russian Revolver, 7 chambers.", "a thick russian accent echos in the room.");
                Tools.Dialog("Spin the chamber, and pull the trigger. If you are lucky, you get to play again, but after me.", "said the buff looking gentleman across the small table from you.");
                Tools.Dialog("The first person to die, loses. The first person to win, gets to leave.", "He continued.");
                Tools.Story("Well.. here goes nothing..\n\n");

                Game game = new Game();
                game.start();
            } else if(action == "mp")
            {
                MultiplayerGame game = new MultiplayerGame();
                game.start();
            }

            Console.WriteLine("\n\nPress any key to exit.");
            Console.ReadKey();
        }
        
    }

}
